package serial

import (
	"github.com/tarm/serial"
	"sync"
	"recive-service/logger"
)

type emulatedPort struct {
	mux sync.Mutex
	config *serial.Config
}

func NewEmulator() serialPort {
	return &emulatedPort{}
}

func (port *emulatedPort) Write(bytes []byte, callback func(byte, error)) {
	callback(0x6, nil)
}

func (port *emulatedPort) Connect() error {
	logger.Info("Connected to emulated port")
	return nil
}

func (port *emulatedPort) Disconnect() {
	logger.Info("Disconnected from emulated port")
}
package serial

import (
	"recive-service/configurations"
	"time"
	"github.com/tarm/serial"
)

type serialPort interface {
	Write([]byte, func(byte, error))
	Connect() error
	Disconnect()
}

var (
	appConfig = configurations.GetConfiguration()
	config = getDefaultPortConfig(appConfig)
)

func getDefaultPortConfig(c *configurations.Configuration) *serial.Config {
	return &serial.Config {
		Name: c.Serial.Name,
		Baud: c.Serial.Speed,
		Size: c.Serial.Size,
		ReadTimeout: time.Millisecond * c.Serial.ReadTimeout,
		StopBits: c.Serial.StopBit,
	}
}
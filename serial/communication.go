package serial

import (
	"recive-service/logger"
)

var (
	port = func() serialPort {
		if appConfig.Serial.Emulate {
			return NewEmulator()
		}
		return NewRealPort(config)
	}()
)

func init() {
	if err := port.Connect(); err != nil {
		logger.Error(err)
	}
}

func WriteToPort(bytes []byte, callback func(byte, error)) {
	port.Write(bytes, callback)
}

func Disconnect() {
	port.Disconnect()
}

func Connect() {
	port.Connect()
}
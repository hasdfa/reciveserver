package serial

import (
	"fmt"
	"recive-service/logger"
	"sync"
	"time"

	"recive-service/configurations"
	"sync/atomic"

	"github.com/tarm/serial"
)

type customPort struct {
	serial         *serial.Port
	buffer         []byte
	mux            sync.Mutex
	reader         chan byte
	errs           chan error
	reconnectAfter int32

	config *serial.Config
}

func NewRealPort(c *serial.Config) serialPort {
	return &customPort{
		config:         c,
		buffer:         make([]byte, 1),
		reader:         make(chan byte),
		errs:           make(chan error),
		reconnectAfter: configurations.GetConfiguration().Serial.ReconnectAfter,
	}
}

var errsCount int32

func (port *customPort) Write(bytes []byte, callback func(byte, error)) {
	port.mux.Lock()
	defer port.mux.Unlock()
	if errsCount > port.reconnectAfter {
		port.Disconnect()
		if err := port.Connect(); err != nil {
			logger.Error(err)
		}
	}
	if err := port.serial.Flush(); err != nil {
		logger.Error(err)
	}

	_, err := port.serial.Write(bytes)
	if err != nil {
		callback(0, err)
		return
	}

	go port.read()
	select {
	case b := <-port.reader:
		callback(b, nil)
		atomic.AddInt32(&errsCount, -errsCount)
	case err = <-port.errs:
		atomic.AddInt32(&errsCount, 1)
		callback(0, err)
	case <-time.Tick(port.config.ReadTimeout * time.Millisecond):
		atomic.AddInt32(&errsCount, 1)
		callback(0, fmt.Errorf("Could not read from serial port"))
	}
}

func (port *customPort) Connect() (err error) {
	port.serial, err = serial.OpenPort(config)
	if err != nil {
		return fmt.Errorf("Unable to connect to serial port: %s", config.Name)
	}
	logger.Infof("Connected to serial port: %s", config.Name)
	return nil
}

func (port *customPort) Disconnect() {
	defer logger.Infof("Disconnected from serial port: %s", config.Name)
	if port.serial == nil {
		return
	}
	defer func() { port.serial = nil }()
	defer port.serial.Close()
	defer port.serial.Flush()
}

func (port *customPort) read() {
	port.buffer = make([]byte, 1)
	n, err := port.serial.Read(port.buffer)
	if err != nil {
		port.errs <- err
	} else if n == 0 || port.buffer[0] != 0x6 {
		port.errs <- fmt.Errorf("Could not read from serial port")
	} else {
		port.reader <- port.buffer[0]
	}
}

package logger

import (
	"os"
	"sync"
	"time"
)

type Lvl uint8

const (
	INFO Lvl = iota + 1
	ERROR
)

func (l *Logger) logToFile(v Lvl, msg string) {
	if !l.fileLogin {
		return
	}
	if fileLogger, ok := loggers[v]; ok && fileLogger != nil {
		fileLogger.log(msg)
	}
}

func (l *Logger) enableFileLogging() {
	loggers = map[Lvl]*fileLogger{
		INFO:  createFileLogger("info.log"),
		ERROR: createFileLogger("error.log"),
	}
}

var (
	startFolder = "logs/" + time.Now().Format("Mon Jan _2 15-04-05 2006")

	loggers = map[Lvl]*fileLogger{}
)

func createFileLogger(filename string) *fileLogger {
	os.Mkdir("logs", os.ModePerm)
	os.Mkdir(startFolder, os.ModePerm)

	filename = startFolder + "/" + filename
	_, err := os.Open(filename)
	if err != nil {
		os.Create(filename)
	}
	return &fileLogger{filename: filename}
}

type fileLogger struct {
	mutex    sync.Mutex
	filename string
}

func (fl *fileLogger) log(msg string) {
	fl.mutex.Lock()
	defer fl.mutex.Unlock()

	file, err := os.OpenFile(fl.filename, os.O_APPEND|os.O_WRONLY, os.ModeAppend)
	if err != nil {
		return
	}
	defer file.Close()

	_, err = file.WriteString(msg + "\r\n")
	if err != nil {
		return
	}
	return
}

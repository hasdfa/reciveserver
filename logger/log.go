package logger

import (
	"fmt"
	"recive-service/utils"
	"sync"
	"time"
)

type (
	Logger struct {
		Prefix    string
		mutex     sync.Mutex
		fileLogin bool
	}
)

var (
	global = NewLogger("background")
)

func NewLogger(pref string) *Logger {
	l := &Logger{Prefix: pref}
	l.EnableFileLogging()
	return l
}

func NewServerLogger() *Logger {
	return NewLogger("server")
}

func NewConnectionLogger(ip string) *Logger {
	return NewLogger(ip)
}

func NewServiceLogger() *Logger {
	return NewLogger("ws-service")
}

func (l *Logger) EnableFileLogging() {
	l.fileLogin = true
	l.enableFileLogging()
}

func (l *Logger) Print(i ...interface{}) {
	l.log(0, "", i...)
}

func (l *Logger) Printf(format string, args ...interface{}) {
	l.log(0, format, args...)
}

func (l *Logger) Info(i ...interface{}) {
	l.log(INFO, "%s", Join(i...))
}

func (l *Logger) Infof(format string, args ...interface{}) {
	l.log(INFO, format, args...)
}

func (l *Logger) Error(i ...interface{}) {
	l.log(ERROR, "%s", Join(i...))
}

func (l *Logger) Errorf(format string, args ...interface{}) {
	l.log(ERROR, format, args...)
}

func (l *Logger) Fatal(i ...interface{}) {
	l.Error(i...)
	utils.PanicExit()
}

func (l *Logger) Fatalf(format string, args ...interface{}) {
	l.Errorf(format, args...)
	utils.PanicExit()
}

func Print(i ...interface{}) {
	global.Print(i...)
}

func Printf(format string, args ...interface{}) {
	global.Printf(format, args...)
}

func Info(i ...interface{}) {
	global.Info(i...)
}

func Infof(format string, args ...interface{}) {
	global.Infof(format, args...)
}

func Error(i ...interface{}) {
	global.Error(i...)
}

func Errorf(format string, args ...interface{}) {
	global.Errorf(format, args...)
}

func Fatal(i ...interface{}) {
	global.Fatal(i...)
}

func Fatalf(format string, args ...interface{}) {
	global.Fatalf(format, args...)
}

func (l *Logger) log(v Lvl, format string, args ...interface{}) {
	message := fmt.Sprintf("%s [%s]: ", time.Now().Format(time.Stamp), l.Prefix) + fmt.Sprintf(format, args...)

	l.mutex.Lock()
	defer l.mutex.Unlock()

	fmt.Println(message)
	l.logToFile(v, message)
}

func Join(ii ...interface{}) string {
	s := ""
	for _, i := range ii {
		if str, c := i.(string); c {
			s += str
		} else if str, c := i.(fmt.Stringer); c {
			s += str.String()
		} else {
			s += fmt.Sprintf("%s", i)
		}
		s += " "
	}
	return s
}

package main

import (
	"recive-service/tcp"
)

func main() {
	server := tcp.NewServer()
	defer server.StopServer()
	server.StartServer()
}

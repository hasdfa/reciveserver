package connection_test

import (
	"testing"
	"net"
	"strconv"
	"fmt"
)

const (
	start = 0x1
	bodyEnd = 0x14
	end = 0x4

	checksum = 0x7d

	localhost = "192.168.1.246:3030"
)

var body = []byte("5000 186565E11000000")

func BenchmarkName(b *testing.B) {
	for i := 0; i < b.N; i++ {
		go func() {
			conn, err := net.Dial("tcp", localhost)
			if err != nil { b.Fatal(err) }
			defer conn.Close()

			body := append(append([]byte{ start }, body...), bodyEnd, checksum, end)
			n, err := conn.Write(body)
			if err != nil { b.Fatal(err) }
			if n != len(body) { b.Fatal("not all bytes have signed up: wanted 24, gived: " + strconv.Itoa(n)) }

			buffer := make([]byte, 3)
			n, err = conn.Read(buffer)
			if err != nil { b.Fatal(err) }
			if n != 3 { b.Fatal("invalid count of bytes have been read: wanted 3, gived: " + strconv.Itoa(n)) }

			if buffer[1] != 0x6 { b.Fatal("invalid result" + strconv.Itoa(n)) }
		}()
	}
}

func TestSimpleConnection(t *testing.T) {
	conn, err := net.Dial("tcp", localhost)
	if err != nil { t.Fatal(err) }
	defer conn.Close()

	body := append(append([]byte{ start }, body...), bodyEnd, checksum, end)
	n, err := conn.Write(body)
	if err != nil { t.Fatal(err) }
	if n != len(body) { t.Fatal("not all bytes have signed up: wanted 24, gived: " + strconv.Itoa(n)) }

	buffer := make([]byte, 3)
	n, err = conn.Read(buffer)
	if err != nil { t.Fatal(err) }
	if n != 3 { t.Fatal("invalid count of bytes have been read: wanted 3, gived: " + strconv.Itoa(n)) }
	fmt.Printf("Result: 0x%x 0x%x 0x%x\n", buffer[0], buffer[1], buffer[2])

	if buffer[1] != 0x6 { t.Fatal("inalid result" + strconv.Itoa(n)) }
}
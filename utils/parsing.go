package utils

type Request struct {
	Raw      [21]byte
	Checksum byte

	AccountCode [4]byte
	Qualifier   byte
	EventCode   [3]byte

	Group [2]byte
	Zone  [3]byte
}

func Parse(bytes []byte) *Request {
	r := &Request{
		Raw:         [21]byte{},
		AccountCode: [4]byte{},
		EventCode:   [3]byte{},
		Group:       [2]byte{},
		Zone:        [3]byte{},
	}

	i := -1
	for _, b := range bytes {
		if b == 1 {
			i = 0
			continue
		} else if b == 4 {
			break
		}

		if i >= 0 {
			if i < 21 {
				r.Raw[i] = b
			} else if i == 21 {
				i = -1
				r.Checksum = b
			}
			i++
		}
	}
	return r
}

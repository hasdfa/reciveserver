package utils

const (
	SOH = 0x01
    EOT = 0x04
	BCC_msk = 0x80
)

func bcc(data [21]byte) byte {
	var bcc byte = 0
	for i := 0; i < 21; i++ {
		bcc ^= data[i]
	}

	if bcc == SOH || bcc == EOT {
		bcc = bcc | BCC_msk
	}

	return bcc
}

func (r *Request) CompareChecksum() bool {
	return r.Checksum == bcc(r.Raw)
}

func (r *Request) GetChecksum() byte {
	return bcc(r.Raw)
}
package utils

import "os"

func IsFileExists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil { return true, nil }
	if os.IsNotExist(err) { return false, nil }
	return true, err
}

func CreateFile(filename string) {
	_, err := os.Open(filename)
	if err != nil { os.Create(filename) }
}
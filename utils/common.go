package utils

import (
	"net"
	"os"
	"strings"
)

func PanicExit() {
	os.Exit(-1)
}

func GetIP() string {
	name, err := os.Hostname()
	if err != nil {
		return ""
	}

	addrs, err := net.LookupHost(name)
	if err != nil {
		return ""
	}

	for _, address := range addrs {
		if strings.Count(address, ".") == 3 && len(address) >= 7 && len(address) <= 15 {
			return address
		}
	}
	return ""
}

//func GetIP() string  {
//	addrs, err := net.InterfaceAddrs()
//	if err != nil {
//		return ""
//	}
//	for _, address := range addrs {
//		// check the address type and if it is not a loopback the display it
//		fmt.Println(address)
//		if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
//			if ipnet.IP.To4() != nil {
//				return ipnet.IP.String()
//			}
//		}
//	}
//	return ""
//}

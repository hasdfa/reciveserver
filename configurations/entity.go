package configurations

import (
	"encoding/json"
	"io/ioutil"
	"recive-service/logger"
	"time"

	"github.com/tarm/serial"
)

const (
	ConfigurationFilePath = "configuration.json"
)

type Configuration struct {
	Tcp     struct {
		Port    string        `json:"port"`
		Timeout time.Duration `json:"timeout"`
	} `json:"tcp"`
	Serial  struct {
		Name          string          `json:"name"`
		Speed         int             `json:"speed"`
		Size          byte            `json:"size"`
		ReadTimeout   time.Duration   `json:"read_timeout"`

		StopBit       serial.StopBits `json:"-"`
		StringStopBit string          `json:"stop_bit"`

		ReconnectAfter int32          `json:"reconnect_after_err_times"`
		Emulate       bool            `json:"emulate"`
	} `json:"serial_port"`
	Service struct {
		Name          string          `json:"name"`
		DisplayName   string          `json:"display_name"`
	} `json:"service"`
}

// Singleton configuration
var conf *Configuration = nil

func GetConfiguration() *Configuration {
	if conf == nil {
		conf = shouldParseConfiguration()
	}
	return conf
}

func shouldParseConfiguration() *Configuration {
	var file []byte
	var err error
	if file, err = ioutil.ReadFile(ConfigurationFilePath); err != nil {
		logger.Fatal(err)
	}
	var c *Configuration
	if err = json.Unmarshal(file, &c); err != nil {
		logger.Fatal(err)
	}
	return shouldValidateConfiguration(c)
}

func shouldValidateConfiguration(c *Configuration) *Configuration {
	if !c.Serial.Emulate {
		switch c.Serial.StringStopBit {
		case "1":
			c.Serial.StopBit = serial.Stop1
		case "1.5":
			c.Serial.StopBit = serial.Stop1Half
		case "2":
			c.Serial.StopBit = serial.Stop2
		default:
			logger.Fatal("invalid serial_port.stop_bit value: " + c.Serial.StringStopBit)
		}
	}
	if c.Serial.ReconnectAfter == 0 {
		c.Serial.ReconnectAfter = 5
	}
	// Add more validates
	return c
}

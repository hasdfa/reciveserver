package tcp

import (
	"recive-service/configurations"
	"recive-service/logger"
	"recive-service/utils"
)

type Server interface {
	StartServer()
	StopServer()
}

func NewServer() Server {
	return &CustomServer{
		host: utils.GetIP(),
		config: configurations.GetConfiguration(),
		logger: logger.NewServerLogger(),
	}
}
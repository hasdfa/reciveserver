package tcp

import (
	"net"
	"recive-service/logger"
	"recive-service/serial"
	"recive-service/utils"
	"time"
)

type ServerConnection struct {
	net.Conn
	log      *logger.Logger
	deadline time.Duration
	receiver chan []byte
	isClosed bool
	buffer   []byte
}

func (s *CustomServer) NewConnection(c net.Conn) *ServerConnection {
	return &ServerConnection{
		Conn:     c,
		deadline: s.config.Tcp.Timeout,
		log:      logger.NewConnectionLogger(c.RemoteAddr().String()),
		receiver: make(chan []byte),
		buffer:   make([]byte, 24),
	}
}

var empty = make([]byte, 0)

func (conn *ServerConnection) HandleConnection() {
	defer func() {
		err := recover()
		if err != nil {
			conn.receiver <- empty
			conn.log.Error(err)
		}
	}()

	_, err := conn.Read(conn.buffer)
	if err != nil {
		conn.log.Error(err)
		return
	}

	request := utils.Parse(conn.buffer)
	if request.Raw[0] != 53 {
		conn.log.Info("Invalid request:", string(request.Raw[:21]))
		conn.receiver <- empty
		return
	}
	if !request.CompareChecksum() {
		conn.log.Infof("Invalid checksum of `%s`: given %d, must be %d",
			string(request.Raw[:21]),
			request.Checksum,
			request.GetChecksum(),
		)
		conn.receiver <- empty
		return
	}
	conn.log.Infof("Accepted `%s`", string(request.Raw[:21]))

	serial.WriteToPort(request.Raw[:21], func(b byte, e error) {
		if conn.isClosed {
			return
		}
		if err != nil {
			conn.log.Error(err)
			conn.receiver <- empty
		} else {
			if b == 0x6 {
				conn.receiver <- []byte{0x1, b, 0x4}
			} else {
				conn.receiver <- empty
			}
		}
	})
}

func (conn *ServerConnection) Close() error {
	conn.isClosed = true
	conn.log.Info("Closed")
	return conn.Conn.Close()
}

package tcp

import (
	"net"
	"recive-service/configurations"
	"recive-service/logger"
	"time"
)

const (
	connectionType = "tcp"
)

type CustomServer struct {
	host string

	listener net.Listener
	config   *configurations.Configuration

	logger *logger.Logger
}

func (s *CustomServer) StartServer() {
	l, err := net.Listen(connectionType, s.host+":"+s.config.Tcp.Port)
	s.listener = l
	if err != nil {
		logger.Fatal(err)
	}
	defer l.Close()
	s.logger.Info("Started server at " + s.host + ":" + s.config.Tcp.Port)
	timeout := s.config.Tcp.Timeout * time.Millisecond
	for {
		_conn, err := l.Accept()
		conn := s.NewConnection(_conn)
		if err != nil {
			s.logger.Errorf("Could not accept from %s: %s", conn.RemoteAddr().String(), err)
		} else {
			go func() {
				go conn.HandleConnection()
				select {
				case res := <-conn.receiver:
					if len(res) == 3 {
						n, err := conn.Write(res)
						if err != nil {
							conn.log.Error("Could not write:", err)
						} else if n != len(res) {
							conn.log.Errorf("Could not write 3 byte { 0x1 %x 0x4 }")
						} else {
							conn.log.Infof("Success sent { 0x%x 0x%x 0x%x }", res[0], res[1], res[2])
						}
					}
					conn.Close()
				case <-time.Tick(timeout):
					conn.log.Infof("Timeout: " + timeout.String())
					conn.Close()
				}
			}()
		}
	}
}

func (s *CustomServer) StopServer() {
	s.listener.Close()
}
